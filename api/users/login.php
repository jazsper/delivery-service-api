<?php 
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: books');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Users.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

$users = new Users($db);

$data = json_decode(file_get_contents("php://input"));

$users->username = $data->username;
$users->password = $data->password;

try {
    $data = $users->login(); 
    if($data->rowCount() > 0) {
        $row = $data->fetch(PDO::FETCH_ASSOC);
        // set response code
        http_response_code(200);
        echo json_encode(
            array(
                'message' => 'Login successfully.',
                'token' => $row['token'],
            )
        );
    } else {
        // set response code
        http_response_code(401);
        echo json_encode(
            array('message' => 'Login failed.')
        );
    }
} catch (Exception $e) {
    // set response code
    http_response_code(400);
    echo json_encode(
        array('message' => $e->getMessage())
    ); 
}