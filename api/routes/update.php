<?php 

// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: PUT');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Routes.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate blog books object
$routes = new Routes($db);

// Get raw route data
$data = json_decode(file_get_contents("php://input"));

if (empty($data->token)) {
    echo json_encode(
        array('message' => 'No access token.')
    );
    return false;
  }

// Set ID to update
$routes->id = $data->id;

$routes->start = $data->start;
$routes->end = $data->end;
$routes->time = $data->time;
$routes->cost = $data->cost;
$routes->token = $data->token;
  
$row = $routes->get_role()->fetch(PDO::FETCH_ASSOC);
$role = $row['role'];

try {
    if ($role != 1) {
        // set response code
        http_response_code(403);
        echo json_encode(
            array('message' => 'Cant update. You dont have admin rights.')
        );
    } else {
        if($routes->update_route()) {
            // set response code
            http_response_code(200);
            echo json_encode(
                array('message' => 'Updated successfully.')
            );
        } else {
            // set response code
            http_response_code(401);
            echo json_encode(
                array('message' => 'Update failed.')
            );
        }
    }
} catch (Exception $e) {
    echo json_encode(
        array('message' => $e->getMessage())
    ); 
}