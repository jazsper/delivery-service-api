<?php 
//  $url ='http://localhost/delivery_service/api/routes/create.php';
//  var_dump($_SERVER);
//  die();
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');
  header('Access-Control-Allow-Methods: books');
  header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

  include_once '../../config/Database.php';
  include_once '../../models/Routes.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  $routes = new Routes($db);

  $data = json_decode(file_get_contents("php://input"));

  if (empty($data->token)) {
    echo json_encode(
        array('message' => 'No access token.')
    );
    return false;
  }

  $routes->start = $data->start;
  $routes->end = $data->end;
  $routes->time = $data->time;
  $routes->cost = $data->cost;
  $routes->token = $data->token;
  
  $row = $routes->get_role()->fetch(PDO::FETCH_ASSOC);
  $role = $row['role'];
  
    try {
        if ($role != 1) {
            // set response code
            http_response_code(403);
            echo json_encode(
                array('message' => 'Cant create. You dont have admin rights.')
            );
        } else {
            if($routes->create_route()) {
                // set response code
                http_response_code(200);
                echo json_encode(
                    array('message' => 'Created successfully.')
                );
            } else {
                // set response code
                http_response_code(401);
                echo json_encode(
                    array('message' => 'Create failed.')
                );
            }
        }
    } catch (Exception $e) {
        echo json_encode(
            array('message' => $e->getMessage())
        ); 
    }

