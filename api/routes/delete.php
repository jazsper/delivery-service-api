<?php 

// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Routes.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate route object
$routes = new Routes($db);

// Get raw route data
$data = json_decode(file_get_contents("php://input"));

if (empty($data->token)) {
    echo json_encode(
        array('message' => 'No access token.')
    );
    return false;
  }

// Set ID to delete
$routes->id = $data->id;
$routes->token = $data->token;

$row = $routes->get_role()->fetch(PDO::FETCH_ASSOC);
$role = $row['role'];
// Delete route
try {
    if ($role != 1) {
        // set response code
        http_response_code(403);
        echo json_encode(
            array('message' => 'Cant delete. You dont have admin rights.')
        );
    } else {
        if($routes->delete_route()) {
            // set response code
            http_response_code(200);
            echo json_encode(
                array('message' => 'Deleted successfully.')
            );
        } else {
            // set response code
            http_response_code(401);
            echo json_encode(
                array('message' => 'Delete failed.')
            );
        }
    }
} catch (Exception $e) {
    echo json_encode(
        array('message' => $e->getMessage())
    ); 
}