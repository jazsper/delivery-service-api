<?php 
  
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Routes.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();
// Instantiate blog post object
$routes = new Routes($db);
// View all routes query
$result = $routes->read_route();
// Get row count
$num = $result->rowCount();
// Check if any posts
if($num > 0) {
// Post array
$route_arr = array();
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    extract($row);
    $route_item = array(
    'id' => $id,
    'start' => $start,
    'end' => $end,
    'time' => $time,
    'cost' => $cost
    );
    // Push to "data"
    array_push($route_arr, $route_item);
}
// Turn to JSON & output
echo json_encode($route_arr);
} else {
// No Posts
echo json_encode(
    array('message' => 'No route found.')
);
}