<?php 
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: books');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Routes.php';

// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

$routes = new Routes($db);

$data = json_decode(file_get_contents("php://input"));

$origin = $data->origin;
$destination = $data->destination;


try { 
    $route = $routes->get_route();
    $route_points = array();
    if ($route->rowCount() > 0) {
        // output data of each row
        while($row = $route->fetch(PDO::FETCH_ASSOC)) {
            $start = $row["start"];
            $end = $row["end"];
            $time = $row['time'];
            $cost = $row['cost'];

            $route_points[$start][$end] = [$time, $cost];
        }

        // //initialize the array for storing
        $S = array();//the nearest path with its parent and time
        $Q = array();//the left nodes without the nearest path
        foreach(array_keys($route_points) as $val) $Q[$val] = 99999;
        $Q[$origin] = 0;

        while(!empty($Q)){
            $min = array_search(min($Q), $Q);//the most min time
            if($min == $destination) break;  
            foreach($route_points[$min] as $key=>$val)
                if(!empty($Q[$key]) && $Q[$min] + $val[0] < $Q[$key]) {
                    $Q[$key] = $Q[$min] + $val[0];
                    $S[$key] = array($min, $Q[$key]);
                }
            unset($Q[$min]);
        }
        
        if (!array_key_exists($destination, $S)) {
            echo "Invalid route.";
            return;
        }

        //list the path
        $path = array();
        $pos = $destination;
        while($pos != $origin){
            $path[] = $pos;
            $pos = $S[$pos][0];
        }
        $path[] = $origin;

        // print_r($path);
        $path = array_reverse($path);

        //print result
        echo "From origin: $origin to destination: $destination" . "\n";
        echo "The time is ".$S[$destination][1] . "\n";
        echo "The best route is ".implode('-', $path);

    } else {
        echo "0 results";
    }
} catch (Exception $e) {
    echo json_encode(
        array('message' => $e->getMessage())
    ); 
}