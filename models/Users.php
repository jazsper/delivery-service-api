<?php
    class Users {

        private $conn;
        private $users = 'users';

        public function __construct($db) {
            $this->conn = $db;
        }

        public function login() {
            // Create query
            $query = 'SELECT * FROM ' . $this->users . ' WHERE username = :username AND password = :password';
            //Prepare statement
            $stmt = $this->conn->prepare($query);
            // Bind ID
            $stmt->bindParam(':username', $this->username);
            $stmt->bindParam(':password', $this->password);
            // Execute query
            $stmt->execute();
            return $stmt;
        }
        
    }