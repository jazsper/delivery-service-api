<?php
    class Routes {

        private $conn;
        private $routes = 'routes';
        private $users = 'users';

        public function __construct($db) {
            $this->conn = $db;
        }

        public function create_route() {
            $query = 'INSERT INTO '.$this->routes . ' SET start = :start, end = :end, time = :time, cost = :cost';
            $stmt = $this->conn->prepare($query);

            $this->start = htmlspecialchars(strip_tags($this->start));
            $this->end = htmlspecialchars(strip_tags($this->end));
            $this->time = htmlspecialchars(strip_tags($this->time));  
            $this->cost = htmlspecialchars(strip_tags($this->cost));        

            $stmt-> bindParam(':start', $this->start);
            $stmt-> bindParam(':end', $this->end);
            $stmt-> bindParam(':time', $this->time);
            $stmt-> bindParam(':cost', $this->cost);

            if($stmt->execute()) {
                return true;
            } 
        }

        public function read_route()
        {
            $query = 'SELECT * FROM ' . $this->routes;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }

        public function update_route()
        {
            $query = 'UPDATE ' . $this->routes . '
            SET start = :start, end = :end, time = :time, cost = :cost
            WHERE id = :id';

            $stmt = $this->conn->prepare($query);

            $this->start = htmlspecialchars(strip_tags($this->start));
            $this->end = htmlspecialchars(strip_tags($this->end));
            $this->time = htmlspecialchars(strip_tags($this->time));  
            $this->cost = htmlspecialchars(strip_tags($this->cost));        
            $this->id = htmlspecialchars(strip_tags($this->id));

            $stmt-> bindParam(':start', $this->start);
            $stmt-> bindParam(':end', $this->end);
            $stmt-> bindParam(':time', $this->time);
            $stmt-> bindParam(':cost', $this->cost);
            $stmt->bindParam(':id', $this->id);

            if($stmt->execute()) {
                return true;
            }
                printf("Error: %s.\n", $stmt->error);
                return false;
        }

        public function delete_route() {
            $query = 'DELETE FROM ' . $this->routes . ' WHERE id = :id';
            $stmt = $this->conn->prepare($query);

            $this->id = htmlspecialchars(strip_tags($this->id));

            $stmt->bindParam(':id', $this->id);

            if($stmt->execute()) {
            return true;
            }
            printf("Error: %s.\n", $stmt->error);
            return false;
        }

        public function get_role() {
            $query  = 'SELECT role FROM '.$this->users. ' WHERE token = :token';
            $stmt = $this->conn->prepare($query);

            $this->token = htmlspecialchars(strip_tags($this->token));
            $stmt->bindParam(':token', $this->token);
            
            $stmt->execute();
            return $stmt;
        }

        public function get_route() {
            $query  = 'SELECT start, end, time, cost FROM ' . $this->routes;
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            return $stmt;
        }

    }