<?php

class Database {

  private $host = 'localhost';
  private $db_name = 'delivery_service_db';
  private $username = 'root';
  private $password = '';
  private $conn;

  public function connect() {
      $this->conn = null;
      try { 
        $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        // set the PDO error mode to exception
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo 'Connected successfully.';
      } catch(PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
      }
      return $this->conn;
  }
}