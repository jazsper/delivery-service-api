# README #

## Delivery Service API.

Login user then get the token return by respone.
Role value in db must be 1 to enable user to create, update and delete.

## CREATE ROUTE
http://localhost/delivery-service-api/api/routes/create.php
Parameters
{
	"token": "02b0732024cad6ad3dc2989bc82a1ef5",
	"start": "a",
	"end": "c",
	"time": 1,
	"cost": 12 
}

## READ ROUTE
http://localhost/delivery-service-api/api/routes/read.php

## UPDATE ROUTE
http://localhost/delivery_service/api/routes/update.php
Parameters
{
	"token": "02b0732024cad6ad3dc2989bc82a1ef5",
	"id": 13,
	"start": "s",
	"end": "t",
	"time": 1,
	"cost": 1 
}

## DELETE ROUTE
http://localhost/delivery-service-api/api/routes/delete.php
Parameters
{
	"token": "02b0732024cad6ad3dc2989bc82a1ef5",
	"id": 28
}

## GET FASTEST ROUTE
http://localhost/delivery-service-api/api/routes/get_route.php
Parameters
{
	"origin": "a",
	"destination": "b"
}